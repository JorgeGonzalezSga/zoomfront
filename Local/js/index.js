import {ZoomMtg} from "@zoomus/websdk";
const axios = require('axios').default;


console.log("checkSystemRequirements");
console.log(JSON.stringify(ZoomMtg.checkSystemRequirements()));

// it's option if you want to change the WebSDK dependency link resources. setZoomJSLib must be run at first
// if (!china) ZoomMtg.setZoomJSLib('https://source.zoom.us/1.7.10/lib', '/av'); // CDN version default
// else ZoomMtg.setZoomJSLib('https://jssdk.zoomus.cn/1.7.10/lib', '/av'); // china cdn option
// ZoomMtg.setZoomJSLib('http://localhost:9999/node_modules/@zoomus/websdk/dist/lib', '/av'); // Local version default, Angular Project change to use cdn version
ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();

// const API_KEY = "NVugqe0IRCuxHXlHJ-IyCw";

/**
 * NEVER PUT YOUR ACTUAL API SECRET IN CLIENT SIDE CODE, THIS IS JUST FOR QUICK PROTOTYPING
 * The below generateSignature should be done server side as not to expose your api secret in public
 * You can find an eaxmple in here: https://marketplace.zoom.us/docs/sdk/native-sdks/Web-Client-SDK/tutorial/generate-signature
 */
// const API_SECRET = "GR659Xl8BubhuWdNGFaIf1nmInsTF4K1GuM7";
// const JWT = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6Ik5WdWdxZTBJUkN1eEhYbEhKLUl5Q3ciLCJleHAiOjE1OTU4NTU1NTEsImlhdCI6MTU5NTI1MDc1MX0.gw5n0hpFwKdSFuRVGWXMUjEKwmo-7ZBAvSGOZbsAZJw"
testTool = window.testTool;
// document.getElementById("display_name").value =
//     "Local" +
//     ZoomMtg.getJSSDKVersion()[0] +
//     testTool.detectOS() +
//     "#" +
//     testTool.getBrowserInfo();
// document.getElementById("meeting_number").value = testTool.getCookie(
//     "meeting_number"
// );
// document.getElementById("meeting_pwd").value = testTool.getCookie(
//     "meeting_pwd"
// );
// if (testTool.getCookie("meeting_lang"))
//     document.getElementById("meeting_lang").value = testTool.getCookie(
//         "meeting_lang"
//     );
//
// document.getElementById("meeting_lang").addEventListener("change", (e) => {
//     testTool.setCookie(
//         "meeting_lang",
//         document.getElementById("meeting_lang").value
//     );
//     $.i18n.reload(document.getElementById("meeting_lang").value);
//     ZoomMtg.reRender({lang: document.getElementById("meeting_lang").value});
// });
//
// document
//     .getElementById("meeting_number")
//     .addEventListener("input", function (e) {
//         let tmpMn = e.target.value.replace(/([^0-9])+/i, "");
//         if (tmpMn.match(/([0-9]{9,11})/)) {
//             tmpMn = tmpMn.match(/([0-9]{9,11})/)[1];
//         }
//         let tmpPwd = e.target.value.match(/pwd=([\d,\w]+)/);
//         if (tmpPwd) {
//             document.getElementById("meeting_pwd").value = tmpPwd[1];
//             testTool.setCookie("meeting_pwd", tmpPwd[1]);
//         }
//         document.getElementById("meeting_number").value = tmpMn;
//         // testTool.setCookie(
//         //     "meeting_number",
//         //     document.getElementById("meeting_number").value
//         // );
//     });


$(document).ready(function () {
    var pathname = window.location.pathname; // Returns path only (/path/example.html)
    var url = window.location.href;     // Returns full URL (https://example.com/path/example.html)
    var origin = window.location.origin;   //
    const meeting = getUrlParameter("m");
    const user = getUrlParameter("u");
    const backendHost = "https://eis01.com";
    const backendPath = "/meeting/add/"
    const backendUrl = backendHost + backendPath + meeting + "/" + user +"?XDEBUG_SESSION_START=PHPSTORM";
    const meetingConfig = {
        mn: null,
        pwd: null,
        role: 0,
        lang: "es-ES",
        signature: "",
        china: 0,
        name : null,
        email : null,
    }

    axios.get(backendUrl).then(function (response) {
        console.log(response)
        const meetingNumber = response.data.meetingNumber
        const leaveUrl = response.data.leaveUrl +"?XDEBUG_SESSION_START=PHPSTORM";
        const password = response.data.password
        const startUrl = response.data.start_url
        const actorRole = response.data.meetingActorRole
        const apiKey = response.data.apiKey
        const userName = response.data.userName
        const userEmail = response.data.userEmail
        const apiKeySecret = response.data.apiKeySecret
        const signature = ZoomMtg.generateSignature({
            meetingNumber: meetingNumber,
            apiKey: apiKey,
            apiSecret: apiKeySecret,
            role: actorRole,
            leaveUrl: leaveUrl ,
            name : userName,
            email : userEmail,
            success: function (res) {
                meetingConfig.mn = meetingNumber
                meetingConfig.pwd = password
                meetingConfig.leaveUrl = leaveUrl
                meetingConfig.name = userName
                meetingConfig.email = userEmail
                console.log(res.result);
                meetingConfig.signature = res.result;
                meetingConfig.apiKey = apiKey;
                const joinUrl = "/meeting.html?" + testTool.serialize(meetingConfig);
                console.log("LEAVE URL" + meetingConfig.leaveUrl)
                testTool.createZoomNode("websdk-iframe", joinUrl);
            },
            error: function (err) {
                console.log(err)
            }
        });
    }).catch(function (err) {
        console.log(err)
    })

});


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};


// document.getElementById("clear_all").addEventListener("click", (e) => {
//     testTool.deleteAllCookies();
//     document.getElementById("display_name").value = "";
//     document.getElementById("meeting_number").value = "";
//     document.getElementById("meeting_pwd").value = "";
//     document.getElementById("meeting_lang").value = "en-US";
//     document.getElementById("meeting_role").value = 0;
//     window.location.href = "/index.html";
// });

//
// document.getElementById("createMeeting").addEventListener("click", (e) => {
//     e.preventDefault();
//     const url = "http://localhost/eis01/public/zoom?XDEBUG_SESSION_START=PHPSTORM";
//
//     const meetingConfig = testTool.getMeetingConfig();
//     console.log(meetingConfig)
//     axios.post(url,
//         JSON.stringify({
//             topic: 'New Meeting at 2',
//             jwt: JWT,
//             type: 1,
//             start_time: '2019-08-22T14:00:00Z',
//             duration: 45
//         })).then(function (response) {
//         console.log(response);
//         const meetingNumber = response.data.meetingId
//         console.log(meetingNumber)
//         const password = response.data.password
//         const startUrl = response.data.start_url
//
//         console.log(password)
//         const signature = ZoomMtg.generateSignature({
//             meetingNumber: meetingNumber,
//             apiKey: API_KEY,
//             apiSecret: API_SECRET,
//             role: 1,
//             success: function (res) {
//                 meetingConfig.mn = meetingNumber
//                 meetingConfig.pwd = password
//                 console.log(res.result);
//                 meetingConfig.signature = res.result;
//                 meetingConfig.apiKey = API_KEY;
//                 console.log(meetingConfig)
//                 const joinUrl = "/meeting.html?" + testTool.serialize(meetingConfig);
//                 console.log(joinUrl);
//                 testTool.createZoomNode("websdk-iframe", joinUrl);
//             },
//         });
//
//         const joinUrl = response.data.joinUrl
//     }).catch(function (err) {
//         console.log(err)
//     })
// })
//
// document.getElementById("joinUser").addEventListener("click", (e) => {
//     e.preventDefault();
//     const url = "http://localhost/eis01/public/getZoom?XDEBUG_SESSION_START=PHPSTORM";
//
//     const meetingConfig = testTool.getMeetingConfig();
//     console.log(meetingConfig)
//     axios.post(url,
//         JSON.stringify({
//             jwt: JWT,
//         })).then(function (response) {
//         console.log(response);
//         const meetingNumber = response.data.meetingId
//         console.log(meetingNumber)
//         const password = response.data.password
//         const startUrl = response.data.start_url
//
//         console.log(password)
//         const signature = ZoomMtg.generateSignature({
//             meetingNumber: meetingNumber,
//             apiKey: API_KEY,
//             apiSecret: API_SECRET,
//             role: 0,
//             success: function (res) {
//                 meetingConfig.mn = meetingNumber
//                 meetingConfig.pwd = password
//                 console.log(res.result);
//                 meetingConfig.signature = res.result;
//                 meetingConfig.apiKey = API_KEY;
//                 console.log(meetingConfig)
//                 const joinUrl = "/meeting.html?" + testTool.serialize(meetingConfig);
//                 console.log(joinUrl);
//                 testTool.createZoomNode("websdk-iframe", joinUrl);
//             },
//         });
//
//         const joinUrl = response.data.joinUrl
//     }).catch(function (err) {
//         console.log(err)
//     })
// })
//
//
// document.getElementById("join_meeting").addEventListener("click", (e) => {
//     e.preventDefault();
//     const url = "http://localhost/eis01/public/zoom?XDEBUG_SESSION_START=PHPSTORM";
//
//     const meetingConfig = testTool.getMeetingConfig();
//
//     console.log(meetingConfig)
//     axios.post(url,
//         JSON.stringify({
//             topic: 'New Meeting at 2',
//             jwt: JWT,
//             type: 1,
//             start_time: '2019-08-22T14:00:00Z',
//             duration: 45
//
//         })).then(function (response) {
//         console.log(response);
//         const meetingNumber = response.data.meetingId
//         console.log(meetingNumber)
//         const password = response.data.password
//         const startUrl = response.data.start_url
//
//         console.log(password)
//         const signature = ZoomMtg.generateSignature({
//             meetingNumber: meetingNumber,
//             apiKey: API_KEY,
//             apiSecret: API_SECRET,
//             role: meetingConfig.role,
//             success: function (res) {
//                 meetingConfig.mn = meetingNumber
//                 meetingConfig.pwd = password
//                 // testTool.setCookie("meeting_number",meetingNumber);
//                 // testTool.setCookie("meeting_pwd", password);
//                 console.log(res.result);
//                 meetingConfig.signature = res.result;
//                 meetingConfig.apiKey = API_KEY;
//                 console.log(meetingConfig)
//                 const joinUrl = "/meeting.html?" + testTool.serialize(meetingConfig);
//                 console.log(joinUrl);
//                 window.open(joinUrl, "_blank");
//             },
//         });
//
//         const joinUrl = response.data.joinUrl
//     }).catch(function (err) {
//         console.log(err)
//     })
// });
//
// // click copy jon link button
// window.copyJoinLink = function (element) {
//     const meetingConfig = testTool.getMeetingConfig();
//     if (!meetingConfig.mn || !meetingConfig.name) {
//         alert("Meeting number or username is empty");
//         return false;
//     }
//     const signature = ZoomMtg.generateSignature({
//         meetingNumber: meetingConfig.mn,
//         apiKey: API_KEY,
//         apiSecret: API_SECRET,
//         role: meetingConfig.role,
//         success: function (res) {
//             console.log(res.result);
//             meetingConfig.signature = res.result;
//             meetingConfig.apiKey = API_KEY;
//             const joinUrl =
//                 testTool.getCurrentDomain() +
//                 "/meeting.html?" +
//                 testTool.serialize(meetingConfig);
//             $(element).attr("link", joinUrl);
//             const $temp = $("<input>");
//             $("body").append($temp);
//             $temp.val($(element).attr("link")).select();
//             document.execCommand("copy");
//             $temp.remove();
//         },
//     });
// };
//
// // click join iframe buttong
// document.getElementById("join_iframe").addEventListener("click", function (e) {
//     e.preventDefault();
//     const meetingConfig = testTool.getMeetingConfig();
//     if (!meetingConfig.mn || !meetingConfig.name) {
//         alert("Meeting number or username is empty");
//         return false;
//     }
//     const signature = ZoomMtg.generateSignature({
//         meetingNumber: meetingConfig.mn,
//         apiKey: API_KEY,
//         apiSecret: API_SECRET,
//         role: meetingConfig.role,
//         success: function (res) {
//             console.log(res.result);
//             meetingConfig.signature = res.result;
//             meetingConfig.apiKey = API_KEY;
//             const joinUrl =
//                 testTool.getCurrentDomain() +
//                 "/meeting.html?" +
//                 testTool.serialize(meetingConfig);
//             testTool.createZoomNode("websdk-iframe", joinUrl);
//         },
//     });
// });
